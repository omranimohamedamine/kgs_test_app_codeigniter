<div class="container">
    <div class="row">
        <div class="col-12 col-sm8- offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper">
            <div class="container">
                <h3>KGS-Dashboard</h3>
                <hr>

                <?php if (session()->get('success')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?= session()->get('success') ?>
                    </div>
                <?php endif; ?>
                <form class="" action="<?php echo base_url('/dashboard') ?>" method="post">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="tel">Tel Number</label>
                                <input type="text" class="form-control" name="tel" id="tel" value="<?= set_value('tel') ?>">
                            </div>
                        </div>
                        <br/>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" name="message" id="message" value="<?= set_value('message') ?>">

                                </textarea>
                            </div>
                        </div>

                        <?php if (isset($validation)) : ?>
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= $validation->listErrors() ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <button type="submit" class="btn btn-primary">SEND</button>
                            <a href="<?php echo base_url('/logout') ?>"> <button type="button" class="btn btn-primary">logout</button></a>
                        </div>

                    </div>
                </form>


            </div>
        </div>
    </div>
</div>