<?php namespace App\Controllers;
//defined('BASEPATH') OR exit('No direct script access allowed');
use Twilio\Rest\Client;
class Dashboard extends BaseController
{
	public function index()
	{
		$data = [];
		helper(['form']);

		if ($this->request->getMethod() == 'post') {
			//let's do the validation here
			$rules = [
				//'tel' => 'required|regex_match[/^[0-9]{14}$/]',
				'tel' => 'required|min_length[12]|max_length[12]',	
				'message' => 'required|min_length[10]|max_length[255]',				
			];

			if (! $this->validate($rules)) {
				$data['validation'] = $this->validator;
			}else{
				
				$data = [
					'phone' => $this->request->getVar('tel'),
					'text' => $this->request->getVar('message'),			
				];
				
				
				$rs='Successful Sent to ';
				$rs =$rs.$data['phone'];
				
				
				//echo $datastr ;
				//$data = ['phone' => '+919703132428', 'text' => 'Hello, CI'];
				print_r($this->sendSMS($data));
				
				session()->setFlashdata('success', $rs);
			
				return redirect()->to( base_url('/dashboard') );

			}
		}

		echo view('templates/header', $data);
		echo view('dashboard');
		echo view('templates/footer');
	}


	protected function sendSMS($data) {
		// Your Account SID and Auth Token from twilio.com/console
		  $sid = 'ACa191b47bf1aad62d21cbee1d0b757373';
		  $token = '66937d55adc5bdd1332e32ad07e1c0de';
		  $client = new Client($sid, $token);
		  
		  // Use the client to do fun stuff like send text messages!
		   return $client->messages->create(
			  // the number you'd like to send the message to
			  $data['phone'],
			  array(
				  // A Twilio phone number you purchased at twilio.com/console
				  "from" => "+12058097038",
				  // the body of the text message you'd like to send
				  'body' => $data['text']
			  )
		  );
  }

	//--------------------------------------------------------------------

}
