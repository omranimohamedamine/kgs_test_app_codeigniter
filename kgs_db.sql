-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : ven. 08 mai 2020 à 08:17
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `kgs_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated-at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `created_at`, `updated-at`) VALUES
(1, 'omrani', 'mohamed amine', 'omranimohamedamine@gmail.com', '$2y$10$G9CIWzfnQm.2xjfG7pup2emmiuUXmHD.RH0Bi/6lBpZJxew/fx/9y', '2020-05-08 00:11:02', '2020-05-08 07:11:02'),
(2, 'omraniomrani', 'omraniomrani', 'omraniomrani@gmail.com', '$2y$10$fPZ89ZdLCH4spJJmdJypkOcxjUhWPV6MVbSObDx2nl3cfy9WJO0Tq', '2020-05-08 00:23:25', '2020-05-08 07:23:25'),
(3, 'Riadh', 'Slimane', 'riadh@gmail.com', '$2y$10$i3Yrk7Z2p2u5xY9Cp8oz3u0vxr9dVVIFR25Gx2k3IRFgIkzMkAfsi', '2020-05-08 00:53:58', '2020-05-08 07:53:58'),
(4, 'Riadh', 'Slimane', 'riadhslimane@gmail.com', '$2y$10$KHFbSUYtRIuOjRsCC4xev.kj0doRV.q0gRIw0cMIAusaoUMM.uXLW', '2020-05-08 00:58:51', '2020-05-08 07:58:51');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
